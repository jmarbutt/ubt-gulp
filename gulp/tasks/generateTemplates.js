var browserSync = require('browser-sync');

var gulp = require('gulp'),
  htmlmin = require('gulp-htmlmin'),
  uglify = require('gulp-uglify'),
  defaultPlumber = require('../util/default.plumber.js'),
  rename = require('gulp-rename'),
  ngTemplates = require('gulp-ng-templates'),
  files = require('../config/files.config.js'),
  gulpConfig = require('../config/gulp.config.js');

gulp.task('generate-templates', generateTemplates);

var defaultConfig  = gulpConfig.generateTemplates || {
  standalone: false
};


function generateTemplates() {
  return gulp.src(files.templates.client)
    .pipe(defaultPlumber())
    .pipe(htmlmin({
      collapseWhitespace: true
    }))
    .pipe(ngTemplates({
      filename: (gulpConfig.angularAppTemplates ||  'client.templates') + '.js',
      standalone: defaultConfig.standalone,
      module: gulpConfig.angularAppTemplates || 'client.templates',
      path: function(path, base) {
        return path.replace(files.baseGulpPath, '');
      }
    }))
    .pipe(uglify())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest(gulpConfig.buildDir + '/app'))
    .pipe(browserSync.reload({
      stream: true
    }));

}
