(function(ng) {
  'use strict';

  ng.module('client.templates', []);

  var client = ng.module('client', [
      'ngResource',
      'ngAnimate',
      'ui.bootstrap',
      'ubt.core.urls',
      'ubt.core.filters',
      'ui.router',
      'client.version',
      'client.templates',
      'client.navbar',
      'client.home',
      'client.styleguide'
  ]);
  client.run(run);

  /* @ngInject */
  function run(ClientVersion, $log, $state, $rootScope) {
    $log.info('Version ' + ClientVersion.display);
    $rootScope.$on("$stateChangeError", $log.error.bind($log));
    $rootScope.$on('$stateChangeStart', redirectState);

    function redirectState(event, toState, toParams, fromState) {
      if (toState.redirectTo) {
        event.preventDefault();
        $state.go(toState.redirectTo, toParams);
      }

  }


}

})(angular);
