(function(ng) {
  'use strict';

  ng.module('client.home')
    .controller('HomeCtrl', HomeCtrl);

  /* @ngInject */
  function HomeCtrl(ClientVersion) {
    var homeVM = this;
    homeVM.version = ClientVersion.display;

  }
})(angular);
