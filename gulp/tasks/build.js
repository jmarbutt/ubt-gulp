var gulp = require('gulp'),
    git= require('gulp-git'),
    runSequence = require('run-sequence'),
    file = require('../../gulp/config/files.config');
    config = require('../../gulp/config/gulp.config');

if(config.debug){
  console.log("Launching build with ", config.build);
}

gulp.task('build', config.build, function(){
});


config.release = config.release || ['bump', 'build', 'add-and-commit', 'tag']; //add tag after we do the commit command
gulp.task('release', function(callback){
  runSequence.apply(this, config.release.concat(callback));
});
