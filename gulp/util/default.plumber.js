var plumber = require("gulp-plumber");

function  logAndEndStream(error) {
	console.error(error.message);
	this.emit('end');
}

function getDefaultPlumberStream(){
	return 	plumber(logAndEndStream);
}

module.exports = getDefaultPlumberStream;
