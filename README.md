
# What is this repository for? #

A quick and simple gulp build system.  Its designed to get you up and running quickly with gulp.  

## Create base directory

If you already have a directory great.  Make sure you have a `package.json` file.  If you dont you can create it by running `npm init`

## Install ubt-gulp

``` sh
  npm install ubt-gulp  --save-dev
```

## Create a  `gulpfile.js` file in the root of your app:

``` js

var gulp = require('gulp');
require('ubt-gulp')(gulp);

```



If you have additional gulp tasks that are specific to a project, you can add them to your Gulpfile like normal.



# Automated setup.

Want to get up and running fast?  Well it's as simple as typing in:

1. `gulp scaffold`  -  This will basically create a scaled down version of the app which you can customize as you go.
2.  `npm install` - Install all the nessessary dependencies
3.  `bower install ` - Install all js libraries
4.  `gulp` - Run the app!



 But please read the rest of this document so you can fully take advantage of everything we have to offer.

If you dont want to automate it... feel free to follow the following steps:


## Create  a `files.config.js` in your base directory.  

This file holds all of the globs necessary to orchestrate the build process. THe most simple configuration can be see in the `files.config.template.js` file. The abbreviated view is below:

``` js

var defaults = {
    baseGulpPath: __dirname,
    styles: {
      client: [], /* all files you want scss to process generally there is just one file */
      watch: [] /* files you want to watch for chagnes and reload */
    },
    index:['index.html'], /* inject will process this file */
    scripts: {
      client: ['app/**/_*.js','app/**/*.js'], /* bundle for client.min.js */
      test: ['app/**/*.spec.js', 'app/**/*.*.spec.js'], /* location of all spec files */
      dist: []    /* files to create dist.min.js */
    },
    fonts: [    ], /* fonts, these all get copied to the fonts directory (path is mirrored ) */
    assets:[], /* these all get copied to the build directory. e.g. web.config, favicon.ico ,etc  */
    templates:{
      client: ['app/**/*.html'], /*  the all get bundled into client.templates.min.js */
    }
};

```
With the basic setup above you can get up and running quickly.  There are some more advanced usage which you can see toward the end of the document.  


## Create `gulp.config.js`

The gulp.config.js file is used to configure the default gulp tasks.  By default the system will use everything you see in the `gulp/config/gulp.config.js` file.  It will do 90% of what you need.  But in case you want to alter what is passed to the core tasks this is the place for it.  The most important settings to configure is the `default` and `build` tasks.  In the advanced section you can see more about how to use the `version` and `inject` tasks.  The both offer some interesting extensibility.

``` js


var defaults  = {
  default: ['browserSync', 'tdd', 'watch'],
  build: [  'generate-templates',
            'styles',
            'scripts',
            'copy',
            'images',
            'inject'],
  debug: false,  /* if set to true it will make it easier to debug code */
  debugDist: false
};

module.exports = defaults;
```

## Create `app.config.js`

The app.config.js file is a settings file.  It allows you to pass various settings to your app.  This is very useful in situations where you want to change defaults in your app.  e.g. maybe for test you want to hit `api.test.com/api` and for production you want to use `api.com/api` .    This is done via "profiles" and all of this information is turned into a constant that can be leveraged inside of your app.  

Sample `app.config.js` file:


```  js

var _ = require("lodash");


var defaults = {
  PROFILE: getProfile(), //Test, LocalTest, Production, or Local
  LOGIN_PANEL_TEXT:'Client Portal',
};

defaults.profileVersions = {
  "Test": {
    API: "test.api.com/api"
  },

  "Production": {
    API: "api.com/api"
  },

  "Local": {
    DEBUG: true,
    API: getVM()+ "/api"
  }
};

function getVM(){
  return process.env.VM_IP || 'localhost';
}

function getProfile(){
  return process.env.PROFILE || 'Test';
}

module.exports = defaults;

```


You will notice, the `getProfile` function will first read from a system variable called PROFILE, if its there it will use that, if not it will fall back to test.  


# Advanced usage

## Debug mode

### If you like reading minified code read skip this section.  

**Good** you stuck around.  So by default the app runs in "minified mode."  Minified explained by SCSS is compressed, app & dist js is minified and uglified, angular templates are read through template cache.  All of those things make it really easy to debug.  Enter `debug`, `debugStyles`, `debugDist` settings found inside of the `gulp.config.js.`     They all default to **false**.  Flipping them to true (and restarting gulp) will result in an easier to debug env.  The other option is to run `gulp --debug` which sets the `debug` bit for you.

Modes explained:

* **debug** : Expands the `client.min.js` and `client.templates.min.js` files via inject.  Instead of having one file you will see all of your client scripts will be included.  This also disabled the ng-templates so your templates will be read from the filesystem.  Lastly it sets the `debugStyles` which is explained below.  

* **debugStyles** : Disables css minification & enables metadata comments.  This allows you to see what scss file generated the css selector (when you inspect).

* **debugDist** : If for some reason you find your self needing to expand the dist bundle this is your setting.  When you set `debugDist` true it will expand all the files in your `dist.min.js` file.

Gulp flags:
* **--debug** : This is the same as setting `debug:true` in your `gulp.config.js` file.
* **--debug-styles** : This is the same as setting `debugStyles:true` in your `gulp.config.js` file.
* **--debug-dist** : This is the same as setting `debugDist:true` in your `gulp.config.js` file.



## Inject (custom)

This is the hardest one to comprehend without a real use case so I will invent one.  Lets say I have a bower_components/samplelibrary that I'm using.  It has a  `dist/samplelibrary.min.js`  and I'm pretty familiar with the source and I'm actively debugging it. The custom inject task will let me map `samplelibrary.min.js` to all of its individual files.  

In order to set this up you need to do the following:

1. Update `files.config.js` `scripts` section and add a new keys for `samplelibrary` scripts as well as templates (if applicable).  

``` js

      ...
      index:['index.html'],
      scripts: {
        samplelibrary: [ 'bower_components/samplelibrary/file.js' , '...'],
      ...
      templates:{
        samplelibrary: [ 'bower_components/samplelibrary/**/*.html']
      }
```
2. Update your `gulp.config.js` file and add a section for inject:

``` js

  inject:{
    map:[
      {
        name: 'samplelibrary',
        files: files.scripts.samplelibrary,
        html: files.templates.samplelibrary /* this assumes the app also uses ng-tempaltes */
      }
    ]

  },

```

3.  Add an inject section to your `index.html` file that is named the same as your `inject.name` property (in our case `samplelibrary`):

``` html

        <!-- sampleclient:js -->
        <script src="sampleclient.min.js" type="text/javascript"></script>
        <script src="sampleclient.templates.min.js" type="text/javascript"></script>
        <!-- endinject -->

```

4.  Since we are no longer "bundling" this with the other dists we need to add the files to assets so they will get copied to the build folder. Inside of `files.config.js` remove the files from `dist` and add them to `assets`


``` js

      ...
      dist: [
            ...
            //'bower_components/sampleclient/build/sampleclient.min.js',
            //'bower_components/sampleclient/build/sampleclient.templates.min.js',

      ],
      assets:[
        'cordova.js',
        'web.config',
        'favicon.ico',
        'bower_components/sampleclient/build/sampleclient.min.js',
        'bower_components/sampleclient/build/sampleclient.templates.min.js',
      }
```

Volia!  You are done.  Now just launch your gulp in debug mode and you will see your `sampleclient` inject section expanded


## Version

The version pluigin takes the `bower.json` version property and turns it into an angular constant that you can use inside your app.  The usage is relatively simple you basically do nothing (and just include the generate `app/version.js` file in your bundle and inject it into you angular app).  If you want to customize the plugin feel free to add a `version` property to `gulp.config.js`.  By default (settings below) it creates a constant called `ClientVersion`, and a module called `client.version`

e.g.:

``` js
  ... /* rest of gulp.config.js */
  version:{
    path:    files.baseGulpPath + '/bower.json',
    variable: 'ClientVersion',
    constant: 'ClientVersion',
    module: 'client.version'
  },
```
