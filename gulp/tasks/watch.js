var gulp = require('gulp'),
  files = require('../config/files.config.js'),
  config = require('../config/gulp.config'),
  _ = require('lodash'),
  browserSync = require('browser-sync');

var isDebug = config.debug;


gulp.task('watch',['watch-jshint'], watchClient);

gulp.task('copy-inject-files-then-reload' , ['copy-inject-files'], copyThenReload);
gulp.task('copy-inject-html-then-reload' , ['copy-inject-html'], copyThenReload);
gulp.task('copy-templates-then-reload' , ['copy-templates'], copyThenReload);
gulp.task('copy-then-reload' , ['copy-scripts'], copyThenReload);
gulp.task('copy-assets-then-reload' , ['copy-assets'], copyThenReload);


function copyThenReload(){
  browserSync.reload();
  return true;

}

function watchClient(){
  gulp.watch([
    files.styles.watch
  ], ['styles']);

  //if debug is set no one is referencing the real files
  //so we dont want to watch..
  if(isDebug){
    console.log('Starting scripts-client watch');
    gulp.watch([files.scripts.client], ['copy-then-reload']);
    gulp.watch([files.templates.client], ['copy-templates-then-reload']);

    var extensions = config.inject && config.inject.map;
    if(extensions){
      var _files = [];
      _.each(extensions, function(item){
        _files = _files.concat(item.files);
      });
      gulp.watch(_files, ['copy-inject-files-then-reload']);
      var _html = [];
      _.each(extensions, function(item){
        _html = _html.concat(item.html);
      });
      gulp.watch(_html, ['copy-inject-html-then-reload']);
    }



  }else{
    console.log('Starting client-templates watch');
    gulp.watch([files.templates.client], ['generate-templates']);

    console.log('Starting scripts-client watch');
    gulp.watch(files.scripts.client, ['scripts-client']);

  }
  console.log('Starting index / inject watch');
  gulp.watch(['index.html'], ['inject']);

  console.log('Starting watch test');
  gulp.watch([files.scripts.test], ['scripts-test']);

  console.log('Starting watch assets');
  gulp.watch([files.assets], ['copy-assets-then-reload']);

  console.log('Watch setup complete');
  return true;
}
