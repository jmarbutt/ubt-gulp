var gulp = require('gulp'),
    gulpCopy = require('gulp-copy'),
    files = require('../config/files.config.js'),
    _ = require('lodash');

gulp.task('scaffold', scaffold);

function scaffold(){
  //copy the template folder to the "base dir"
  return gulp.src(files.baseGulpPath + 'template/**/*').pipe(gulp.dest('.'));
}
