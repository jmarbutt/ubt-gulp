var url = require('url');

var defaults = {
  PROFILE: getProfile(), //Test, LocalTest, Production, or Local
  LOGIN_PANEL_TEXT:'Client Portal',
};

defaults.profileVersions = {
  "Test": {
    API: "test.api.com/api"
  },

  "Production": {
    API: "api.com/api"
  },

  "Local": {
    DEBUG: true,
    SERVICE_PROTOCOL: getProtocol(),
    SERVICE_IP: getServer() 
  }
};


function ensureTrailingSlash(str){
  return str.replace(/\/?$/, '/');
}

function getServer(){
  var server = process.env.API_SERVER ||  "http://localhost";
  var parsed = url.parse(server);

  return ensureTrailingSlash((parsed.host + (parsed.path || '')) || parsed.path);


}

function getProfile(){
  return process.env.PROFILE || "Test";
}

function getProtocol(){
  if(!process.env.SERVICE_PROTOCOL && process.env.API_SERVER ){
    //see if someone put the protocol in the API_SERVER.. man i'm so notice
    var parsed = url.parse(process.env.API_SERVER );
    if(parsed.protocol){
      return parsed.protocol + '//';
    }
  }
  return process.env.SERVICE_PROTOCOL || "https://";
}

console.log("getProfile is: ", getProfile())
console.log("getServer is: ", getServer())
console.log("getProtocol is: ", getProtocol())

module.exports = defaults;
