
var gulp = require('gulp');
var inject = require('gulp-inject'),
  defaultPlumber = require('../util/default.plumber.js'),
  files = require('../../gulp/config/files.config'),
  gulpConfig = require('../../gulp/config/gulp.config'),
  gulpif = require('gulp-if'),
  _ = require('lodash'),
  browserSync = require('browser-sync');

  function addPipe(stream, item){
    var excludes = ['!**/*.spec.js'];
    return stream.pipe(
            inject(gulp.src(
                excludes.concat(item.files),
                { read: false}),
                {
                  name: item.name,
                  addRootSlash: false
                }
            )
    );
  }

function _customInject(stream, config){
  var extensions = config.inject && config.inject.map;
  if(extensions && config.debug){
    _.each(extensions, function(item){
      stream = addPipe(stream, item);
    });
  }
  return stream;
}

gulp.task('inject', [], function () {

  var excludes = ['!**/*.spec.js'];
  var stream =  gulp.src(files.index)
      .pipe(defaultPlumber())
      .pipe(gulpif(gulpConfig.debugDist,
        inject(gulp.src(excludes.concat(files.scripts.dist), { read: false}),
            { addRootSlash: false, name: 'dist'})
        ))
    .pipe(gulpif(gulpConfig.debug,
      inject(gulp.src(excludes.concat(files.scripts.client), { read: false }), {
          addRootSlash: false
        } )));

  stream = _customInject(stream, gulpConfig);

  return stream.pipe(gulp.dest(gulpConfig.buildDir))
    .pipe(browserSync.reload({
          stream: true
        }));
});
