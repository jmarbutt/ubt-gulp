//pull in the real one if we can find it
var optional = require("optional"),
    url = require("url")
    _ = require("lodash")
    path  = require('path');


var config = optional("./app.config.js"), url  = require('url');


var defaults = {
    PROFILE: getProfile()
  };

  //Settings corresponding to the selected profile
  defaults.profileVersions = {
    "Local": {
      SERVICE_PROTOCOL: getProtocol(),
      SERVICE_IP: getServer() ,
      DEBUG: true,
    }
  };
var merged = _.merge(defaults, config || {});


//This is basically an extend but does not rely on any external libraries
for (var attrName in merged.profileVersions[merged.PROFILE]) {
  merged[attrName] = merged.profileVersions[merged.PROFILE][attrName];
}

merged.SERVICE_ADDRESS = merged.SERVICE_PROTOCOL + merged.SERVICE_IP;

function ensureTrailingSlash(str){
  return str.replace(/\/?$/, '/');
}

function getServer(){
  var server = process.env.API_SERVER ||  "http://localhost";
  var parsed = url.parse(server);

  return ensureTrailingSlash((parsed.host || '') +  (parsed.path || ''));


}

function getProfile(){
  return process.env.PROFILE || "Test";
}

function getProtocol(){
  if(!process.env.SERVICE_PROTOCOL && process.env.API_SERVER ){
    //see if someone put the protocol in the API_SERVER.. man i'm so notice
    var parsed = url.parse(process.env.API_SERVER );
    if(parsed.protocol){
      return parsed.protocol + '//';
    }
  }
  return process.env.SERVICE_PROTOCOL || "https://";
}

if(merged.onInit){
  merged.onInit(merged);
}

module.exports = merged;
