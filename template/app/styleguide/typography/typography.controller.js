(function(angular) {
  'use strict';
  angular.module('client.styleguide')
    .controller('TypographyCtrl', TypographyCtrl);
  /* @ngInject */
  function TypographyCtrl($scope) {
    $scope.textColors =  $scope.colors[0].colors
                          .concat($scope.colors[1].colors)
                          .concat($scope.colors[2].colors)
                          .concat($scope.colors[3].colors);

  }

})(angular);
