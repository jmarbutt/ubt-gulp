var gulp = require('gulp'),
 		gulpConfig = require('../config/gulp.config'),
    del = require('del');

gulp.task('clean-styles', function(cb) {
  del([gulpConfig.buildDir + '/css'], cb);
});
gulp.task('clean-scripts', function(cb) {
  del([gulpConfig.buildDir + '/app'], cb);
});


gulp.task('clean', [], function(cb) {
  del([gulpConfig.buildDir + '/**/*', '!.gitignore'], cb);
});
