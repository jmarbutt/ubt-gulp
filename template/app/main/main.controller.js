(function (ng) {
  'use strict';

  ng.module('client')
    .controller('MainCtrl', MainCtrl);


  /* @ngInject */
  function MainCtrl(NavbarService,ClientVersion) {
    var mainCtrl = this;

    mainCtrl.isNavbarVisible = NavbarService.isVisible;
    mainCtrl.version = ClientVersion.display;

  }

})(angular);
