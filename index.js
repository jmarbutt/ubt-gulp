var fs = require('fs'),
    gulp = require('gulp'),
    requireDir = require('require-dir'),
    _ = require('lodash');

requireDir('./gulp/tasks', { recurse: true });

module.exports = function(theirGulp) {
  theirGulp.tasks = _.merge(gulp.tasks, theirGulp.tasks || {});
};
