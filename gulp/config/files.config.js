//pull in the real one if we can find it
var optional = require("optional"),
    _ = require("lodash"),
    path  = require('path'),
    gulpConfig = optional("./gulp.config.js") || { buildDir: '' };

var config = optional("./files.config");

var basePath = (config && config.baseGulpPath) || ( __dirname + '/../../');

var defaults = {
    baseGulpPath: basePath,
    phoneGap:['app/app.settings.js', "*.xml", 'PhoneGap/**/*', '!PhoneGap/pgbuild-config*'],
    styles: {
      client: ['app/client.scss'],
      watch: ['app/**/*.scss'], //we do this bc the client.scss references files _.scss without this watch it woul not reload
    },
    index:['index.html'],
    assets:['app/{fonts,media,images,js}/**/*.{wav,mp4,svg,png,jpg,jpeg,eot,ttf,svg,woff}'],
    angularApp: 'client',
    scripts: {
      client: [
            'app/version.js',
            'app/**/_.js',
            'app/app.js',
            'app/**/*.js'],
      test: ['app/**/*.spec.js', 'app/**/*.*.spec.js'],
      releaseDist:[
        gulpConfig.buildDir + '/app/dist.min.js'
      ],
      testDist: [
        'bower_components/angular-mocks/angular-mocks.js',
      ],
      dist: [
        'bower_components/jquery/dist/jquery.js',
         'bower_components/jquery-ui/jquery-ui.js',
         'bower_components/angular/angular.js',
         'bower_components/angular-ui-router/release/angular-ui-router.js',
         'bower_components/angular-animate/angular-animate.js',
         'bower_components/angular-sanitize/angular-sanitize.js',
         'bower_components/angular-touch/angular-touch.js',
         'bower_components/fastclick/lib/fastclick.js',
         'bower_components/CryptoJS/build/rollups/pbkdf2.js',
         'bower_components/angular-ui-utils/ui-utils.js',
         'bower_components/bootstrap/dist/app/bootstrap.js',
         'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
         'bower_components/ng-iscroll/src/ng-iscroll.js',
         'bower_components/iscroll/src/iscroll.js',
         'bower_components/momentjs/min/moment.min.js',
         'bower_components/jquery-knob/app/jquery.knob.js',
         'bower_components/accounting/accounting.js',
         'bower_components/venturocket-angular-slider/build/angular-slider.js',
         'bower_components/angular-dragdrop/src/angular-dragdrop.js',

      ]
    },
    templates:{
      client: ['app/**/*.{htm,html}']
    },
    fonts:[
      'bower_components/bootstrap-sass/assets/fonts/**', //i have to do this bc they dont have a standard format one is font/bootstrap and the other is font-awsome/fonts
      'bower_components/font-awesome/fonts/**']

};

function resolvePaths(config){

  if(!config){
    return;
  }


  if(config.baseGulpPath){
    config.baseGulpPath = path.normalize(config.baseGulpPath);
  }
}

if(config && config.scripts){
  if(config.scripts.client){
    delete defaults.scripts.client;
  }
  if(config.scripts.test){
    delete defaults.scripts.test;
  }
  if(config.scripts.dist){
    delete defaults.scripts.dist;
  }
}

module.exports = _.merge(defaults, config || {} );

//resolve paths
resolvePaths(module.exports);
if(config){
  if(!module.exports.baseGulpPath){
    console.error("Must define clientPath (the base path of the client __dirname)");
    throw "Failed to find clientPath files.config.js";
  }else{
    //make sure client path has a trailing slash.
    module.exports.baseGulpPath = module.exports.baseGulpPath.replace(/\/?$/, path.sep);
  }
}
