﻿module.exports = {
  platforms: {
    ios:      false,
    android:  true
  },
  steps: {
    zip:      true,
    build:    true,
    download: true,
    uploadS3: true,
    register: false
  },
  pgbuild: {
    token:                 "<token>",
    appID:                 "<appid>",
    iOSsigningKeyID:       "<key>",
    iOSsigningKeyPassword: "<Password>",
  	debug:                 false
  },
  s3: {
    accessKey:             "<key>",
  	secretAccessKey:       "<secret>",
  	bucket:                "<bucket>",
  	region:                "us-east-1"
  },
  register: {
    apiKey:   "N/A",
    url:      "N/A",
    status:   "O"
  }
};
