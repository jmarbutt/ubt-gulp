var browserSync = require('browser-sync');

var gulp = require('gulp'),
  concat = require('gulp-concat'),
  args = require('yargs').argv,
  gulpif = require('gulp-if'),
  uglify = require('gulp-uglify'),
  defaultPlumber = require('../util/default.plumber.js'),
  reload = browserSync.reload,
  ngAnnotate = require('gulp-ng-annotate'),
  files = require('../config/files.config'),
  config = require('../config/gulp.config');

//todo if we want to clean break it up into smaller pieces e.g clean-core, clean-client
var isDebug = config.debug,
    debugDist = config.debugDist;

gulp.task('scripts', ['scripts-client',
                      'scripts-dist',
                      'scripts-test']);

gulp.task('scripts-client', ['appsettings'],  scriptsClient);
gulp.task('scripts-dist', scriptsDist);
gulp.task('scripts-test', scriptsTest);

/*
  TODO find a clean way to put this in files...
  2cents create a function that looks for name
  return obj[name].concat((obj[name +'Test'] || []).map(funcbelow))
*/

function getScriptsAndFilterOutTests(scripts, tests){
  return tests.map(function(item){
    if(item[0] === '!'){
      return item;
    }
    return '!' + item;
  }).concat(scripts);

}
function scriptsClient() {
  if(isDebug){
    return justBrowswerSyncReload();
  }

  return gulp.src(
    getScriptsAndFilterOutTests(files.scripts.client, files.scripts.test))
    .pipe(defaultPlumber())
    .pipe(ngAnnotate())
    .pipe(gulpif(!isDebug, uglify().on('error', function(e) {
      console.log('\x07', e.message);
      return this.end();
    }))) // skip minification if debug
    .pipe(concat(config.angularApp + '.min.js'))
    .pipe(gulp.dest(config.buildDir + '/app'))
    .pipe(reload({
      stream: true
    }));

}

function justBrowswerSyncReload(){
  reload({
    stream: true
  });
 return true;
}

function scriptsDist() {
  //we only do something if we are not in debug.  If we are in debug
  //we reference the real files
  if(debugDist){
    return justBrowswerSyncReload();
  }

  return gulp.src(files.scripts.dist)
    //.pipe(uglify())
    .pipe(defaultPlumber())
    .pipe(concat('dist.min.js'))
    .pipe(gulp.dest(config.buildDir + '/app'))
    .pipe(reload({
      stream: true
    }));
}

function scriptsTest() {
  return gulp.src(files.scripts.test)
    .pipe(gulp.dest(config.buildDir + '/app'));

}
