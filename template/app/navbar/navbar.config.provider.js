(function(ng) {
  'use strict';


    var DEFAULT_TRAY_DATA = [
      {
        sref: 'home',
        title: 'Home',
        icon: 'fa-home'
      },

      {
        sref: 'styleguide',
        title: 'StyleGuide',
        icon: 'fa-hand-peace-o'
      }
    ];

  ng.module('client.navbar').provider('NavbarConfig', {
    menu: DEFAULT_TRAY_DATA,
    initialize: function(data) {
      this.menu = data;
    },
    $get: function() {
      //"states" is only needed during the config, so we don't include it here
      return this.menu;
    }

  });
})(angular);
