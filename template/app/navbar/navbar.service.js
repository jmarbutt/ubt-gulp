(function (ng) {
  'use strict';

  ng.module('client.navbar').service('NavbarService', NavbarService);

  /* @ngInject */
  function NavbarService ($state, $log, $rootScope) {
    var self = this,
        _visible = false;

    $rootScope.$on('$stateChangeSuccess', onStateChange);

    /**
     * Manually show navbar
     */
    self.show = show;

    /**
     * manually hide navbar
     */
    self.hide = hide;

    /**
     * Get current navbar visibility
     * @type {Boolean}
     */
    self.isVisible = isVisible;

    function show () {
      _visible = true;
    }

    function hide () {
      _visible = false;
    }

    function isVisible () {
      return _visible;
    }

    function onStateChange () {
      if ($state.current && $state.current.config && $state.current.config.navbar && $state.current.config.navbar.hide) {
        self.hide();
      } else {
        self.show();
      }
    }
  }
})(angular);
