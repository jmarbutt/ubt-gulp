var gulp = require('gulp'),
    config = require('../../gulp/config/gulp.config');

if(config.debug){
  console.log("Launching default with ", config.default);
}
gulp.task('default', config.default);
