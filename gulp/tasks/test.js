var gulp = require('gulp'),
    karma = require('karma'),
    files = require('../config/files.config'),
    config = require('../config/gulp.config'),
    fs = require('fs');


gulp.task('tdd', ['scripts-test', 'copy-dist'], function(done) {
  server = new karma.Server(getKarmaConfig(false), done);
  server.start();
});

gulp.task('test', ['scripts-test', 'copy-dist'], function(done) {
    console.log('##########\nTEST: ');
    getFiles().forEach(function(item){
      console.log('\t', item);
    });
    console.log('########');
    server = new karma.Server(getKarmaConfig(true), done);
    server.start();

});


function prefixTestFiles(){
  //every choice we make, makes things more complicated
  //since we "run" our tests from the build dir we have to prefix our
  //test glob with "build", but we watch from js.
  return files.scripts.test.map(function (item){
    if(item[0] === '!'){
      return item;
    }
    return 'build/' + item;
  });
}

function getFiles(){
  var testFiles = [];
  if(config.debug){
    //use non-bundle
    testFiles = testFiles.concat(['build/app/app.settings.js'])
      .concat(files.scripts.dist)
      .concat(files.scripts.testDist)
      .concat(files.scripts.client);
  }else{
    testFiles = testFiles
      .concat(files.scripts.releaseDist)
      .concat(files.scripts.testDist)
      .concat(files.scripts.client);
  }
  return testFiles.concat(prefixTestFiles());
}

function getKarmaConfig(single){
  //start with client.  if it dne fallback to core
  var karmaFile = files.baseGulpPath + 'karma.conf.js';
  console.log("Searching for karma.conf.js", karmaFile);
  if (!fs.existsSync(karmaFile)) {
      // Do something
      karmaFile = __dirname + '/../../karma.conf.js';
      console.warn('Failed to find client karma.conf.js, falling back to core:',karmaFile);
      if(!fs.existsSync(karmaFile)){
        console.error('failed to find kara.conf.js file', karmaFile);
        throw 'test.js: failed to find karma config file';

      }
  }

  return {
    files: getFiles(),
    exclude: ["app/**/*.spec.js", 'bower_components/**/*.spec.js'],
    basePath: files.baseGulpPath,
    configFile:   karmaFile,
    singleRun: single};
}
