(function (ng) {
  'use strict';

  ng.module('client.navbar')
    .directive('navbar', AssistantNavbar)
    .controller('NavbarCtrl', NavbarCtrl);

  function AssistantNavbar () {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/navbar/navbar.directive.html',
      scope: true,
      controller: 'NavbarCtrl',
      controllerAs: 'navbarVM'
    };
  }

  /* @ngInject */
  function NavbarCtrl(NavbarService, NavbarConfig, $state) {
    var navbarVM = this;
    navbarVM.items = NavbarConfig;
    navbarVM.isNavbarVisible = NavbarService.isVisible;
    navbarVM.getNavTitle = getNavTitle;

    function getNavTitle(){
      return $state.current.config && $state.current.config.navbar && $state.current.config.navbar.title;
    }

  }

})(angular);
