var gulp = require('gulp'),
    tag_version = require('gulp-tag-version'),
    config      = require('../config/gulp.config').tag;
    file = require('../config/files.config');
    git = require('gulp-git'),
    bump = require('gulp-bump'),
    args = require('yargs').argv;

config = config || {prefix: ''};

var fileName = './bower.json';

if(_argument('npm')){
  fileName =  './package.json';
}

gulp.task('display', function(){
  var result =  require(file.baseGulpPath + fileName);
  console.log("\n\n####################", result.version, "####################\n\n");
});

gulp.task('tag', function() {
  return gulp.src([fileName]).pipe(tag_version(config));
});

function _argument(name){
  if(config[name] ||  !!~args._.indexOf(name) || args[name]){
    return true;
  }
  return false;
}



// Run git add
// src is the file(s) to add (or ./*)
gulp.task('add-and-commit', function(){
  var result =  require(file.baseGulpPath + './bower.json');

  return gulp.src('.')
    .pipe(git.add())
    .pipe(git.commit('new build commit (' + result.version + ')'));
});



/**
* Bumping version number and tagging the repository with it.
* Please read http://semver.org/
*
* You can use the commands
*
*     gulp patch     # makes v0.1.0 → v0.1.1
*     gulp feature   # makes v0.1.1 → v0.2.0
*     gulp release   # makes v0.2.1 → v1.0.0
*
* To bump the version numbers accordingly after you did a patch,
* introduced a feature or made a backwards-incompatible release.
*/

function inc(importance) {
  // get all the files to bump version in
  if(_argument('major')){
   importance =  'major';
  }
  if(_argument('minor')){
   importance =  'minor';
  }

   // get all the files to bump version in
   var result =  gulp.src([fileName])
       // bump the version number in those files
       .pipe(bump({type: importance}))
       // save it back to filesystem
       .pipe(gulp.dest('./'));
       // commit the changed version number

 if(_argument('autocommit')){
   // **tag it in the repository**
   result = result.pipe(git.commit('bumps package version'));

 }

  if(_argument('autotag')){
    // **tag it in the repository**
    result = result.pipe(tag_version());

  }
  return result;
}

gulp.task('major', function() { return inc('major'); });
gulp.task('minor', function() { return inc('minor'); });
gulp.task('patch', function() { return inc('patch'); });
gulp.task('bump', function() { return inc('bump'); });
