(function(angular) {
  'use strict';
  //TODO move this to its own module.
  angular.module('client.styleguide')
    .directive('insertStyle', InsertStyleDirective);


    /* @ngInject */
    function InsertStyleDirective($timeout, $window){
        return {
          scope:true,
          link: function(scope, element, attrs) {
            $timeout(function(){
              scope.theStyle  = $window.getComputedStyle(element[0], null);
            },500);
          }
        };
    }


})(angular);
