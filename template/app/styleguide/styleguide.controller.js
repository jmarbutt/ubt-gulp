(function(angular) {
  'use strict';
  angular.module('client.styleguide')
    .controller('StyleGuideCtrl', StyleGuideCtrl);
  /* @ngInject */
  function StyleGuideCtrl($templateCache, $scope, $rootScope, $state, $stateParams, $timeout) {

    $scope.colors = [
     {
        name:'Brand',
        colors: ['primary', 'secondary', 'tertiary']
      },

       {
        name : 'Generic',
        colors: ['black', 'white', 'balanced', 'energized', 'stable']
      },
      {
         name : 'Bootstrap',
         colors: ['info', 'success','warning', 'danger']
       },
      {
        name : 'Grays',
       colors : [  'gray-darker','gray-dark','gray-base', 'gray', 'gray-light', 'gray-lighter']
      }
    ];




    $scope.toggleHints = toggleHints;


    $scope.buttonGroup = [{
      name: '.large.btn-one.link',
      selected: true
    }, {
      name: '.large.btn-one.link'
    }, {
      name: '.large.btn-one.link'
    }];

    $scope.tabs = [
      { title:'Title 1', content:'Dynamic content 1' },
      { title:'Title 2', content:'Dynamic content 2', disabled: false }
    ];

    $scope.tabs2 = [
      { title:'Title 1', content:'Dynamic content 1' },
      { title:'Title 2', content:'Dynamic content 2', disabled: false }
    ];

    $scope.showHints = true;

    function toggleHints (){
      $scope.showHints = !$scope.showHints;
    }
     /*carousel ------------------------------------------------------------*/
     $scope.myInterval = 0;
      $scope.noWrapSlides = false;
      var slides = $scope.slides = [];
      $scope.addSlide = function(_index) {
        var newWidth = 600 + slides.length + 1;
        slides.push({
          index:_index,
          title:['Best Fit','Pretty Good Fit','Bad Fit',"Awwwww"][slides.length % 4],
          image: '//placekitten.com/' + newWidth + '/300',
          text: ['Sniffing Kitten','Kitten In Hand','Older Cat',"Basket O' Kitties"][slides.length % 4] + ' '
        });
      };
      for (var i=0; i<4; i++) {
        $scope.addSlide(i);
      }
      $scope.slides.currentIndex = 0;

     $scope.$watch(function () {
        for (var i = 0; i < slides.length; i++) {
          if (slides[i].active) {
            return slides[i];
          }
        }
      }, function (currentSlide, previousSlide) {
        if (currentSlide !== previousSlide) {
          $scope.slides.currentIndex = currentSlide.index;
           console.log(currentSlide);
        // $scope.slides.title = slides[0].title[0];
         // $scope.slides.title = slides.title[currentSlide.index];
           $scope.slides.title = currentSlide.title;
          //console.log('currentSlide:', currentSlide);
        }
      });

    /*---ADD ONS---------------------------------------------------------------------*/

     $scope.addOns = [
      { name:'Add On 1', availability:'Available Online', price:'99.99', img:'/images/tempicon.png' },
      { name:'Add On 2', availability:'Available In Store', price:'199.99', img:'/images/tempicon.png' },
      { name:'Add On 3', availability:'Available In Store', price:'89.99', img:'/images/tempicon.png' }
    ];















    /*OLD CONTENT BELOW ---------------------------------------------------*/


    $scope.rating1 = {
      val: 0
    };
    $scope.rating2 = {
      val: 3
    };
    $scope.rating3 = {
      val: 3.5
    };







    //this is an example of how to create a template inline. imlemented below.
    $templateCache.put('myTemplate.htm', '<div id="Endor">Endor smells of petunias.</div>');

    $scope.productCompareTabs = [{
      title: 'Tab 1',
      active: true,
      contentTemplate: 'myTemplate.htm' //uses $templateCache above
    }];

    //these are examples of the general Confirm.htm modal
    //reference ModalService.ShowConfirmation for options
    var modalOptions1 = {
      title: 'Hello World',
      hideCancel: true,
      message: 'Lorem ipsum dolor sit amet, ea per choro alienum dissentiunt, ea maluisset mnesarchum mel. Ei debitis deterruisset mea, te est omnes maiorum. Cum ignota feugait reformidans ea, sed impedit omittam accumsan no. Mea nobis impedit eu, eos no habeo probatus.',
      buttons: [{
        text: 'Got It'
      }]
    };

    $scope.myModalCall = function() {
      ModalService.ShowConfirmation(modalOptions1);
    };

    var modalOptions2 = {
      style: 'warning',
      title: 'Hello World',
      hideCancel: false,
      message: 'Lorem ipsum dolor sit amet, ea per choro alienum dissentiunt, ea maluisset mnesarchum mel. Ei debitis deterruisset mea, te est omnes maiorum. Cum ignota feugait reformidans ea, sed impedit omittam accumsan no. Mea nobis impedit eu, eos no habeo probatus.',
      buttons: [{
        text: 'Got It',
        icon: 'fa-check'
      }, {
        text: 'More',
        icon: 'fa-paw'
      }, {
        text: 'Cancel',
        icon: 'fa-times'
      }]
    };

    $scope.myModalCall2 = function() {
      ModalService.ShowConfirmation(modalOptions2);
    };

    $scope.featureCompareTabs = [{
      title: 'Tab 1',
      active: true,
      contentTemplate: '/app/modules/builder/templates/featureCompare.html',
      specComparison: [{
        DisplayName: "Product Style 1",
        FeatureName: "productStyle1",
        GroupIndex: 0,
        Product1Value: "Elliptical",
        Product2Value: "Elliptical",
        SortOrder: 10
      }, {
        DisplayName: "Flywheel Weight 1",
        FeatureName: "flywheelWeight1",
        GroupIndex: 0,
        Product1Value: "16 LBS",
        Product2Value: "10 LBS",
        SortOrder: 60
      }, {
        DisplayName: "Drive 1",
        FeatureName: "drive1",
        GroupIndex: 0,
        Product1Value: "Rear",
        Product2Value: "Front",
        SortOrder: 70
      }, {
        DisplayName: "Product Style 2",
        FeatureName: "productStyle2",
        GroupIndex: 0,
        Product1Value: "Elliptical",
        Product2Value: "Elliptical",
        SortOrder: 11
      }, {
        DisplayName: "Flywheel Weight 2",
        FeatureName: "flywheelWeight2",
        GroupIndex: 0,
        Product1Value: "16 LBS",
        Product2Value: "10 LBS",
        SortOrder: 61
      }, {
        DisplayName: "Drive 2",
        FeatureName: "drive2",
        GroupIndex: 0,
        Product1Value: "Rear",
        Product2Value: "Front",
        SortOrder: 71
      }, {
        DisplayName: "Product Style 3",
        FeatureName: "productStyle3",
        GroupIndex: 0,
        Product1Value: "Elliptical",
        Product2Value: "Elliptical",
        SortOrder: 12
      }, {
        DisplayName: "Flywheel Weight 3",
        FeatureName: "flywheelWeight3",
        GroupIndex: 0,
        Product1Value: "16 LBS",
        Product2Value: "10 LBS",
        SortOrder: 62
      }, {
        DisplayName: "Drive 3",
        FeatureName: "drive3",
        GroupIndex: 0,
        Product1Value: "Rear",
        Product2Value: "Front",
        SortOrder: 72
      }, {
        DisplayName: "Product Style 4",
        FeatureName: "productStyle4",
        GroupIndex: 0,
        Product1Value: "Elliptical",
        Product2Value: "Elliptical",
        SortOrder: 13
      }, {
        DisplayName: "Flywheel Weight 4",
        FeatureName: "flywheelWeight4",
        GroupIndex: 0,
        Product1Value: "16 LBS",
        Product2Value: "10 LBS",
        SortOrder: 63
      }, {
        DisplayName: "Drive 4",
        FeatureName: "drive4",
        GroupIndex: 0,
        Product1Value: "Rear",
        Product2Value: "Front",
        SortOrder: 73
      }, {
        DisplayName: "Product Style 5",
        FeatureName: "productStyle5",
        GroupIndex: 1,
        Product1Value: "Elliptical",
        Product2Value: "Elliptical",
        SortOrder: 14
      }, {
        DisplayName: "Flywheel Weight 5",
        FeatureName: "flywheelWeight5",
        GroupIndex: 1,
        Product1Value: "16 LBS",
        Product2Value: "10 LBS",
        SortOrder: 64
      }, {
        DisplayName: "Drive 5",
        FeatureName: "drive5",
        GroupIndex: 1,
        Product1Value: "Rear",
        Product2Value: "Front",
        SortOrder: 74
      }, {
        DisplayName: "Product Style 6",
        FeatureName: "productStyle6",
        GroupIndex: 1,
        Product1Value: "Elliptical",
        Product2Value: "Elliptical",
        SortOrder: 15
      }, {
        DisplayName: "Flywheel Weight 6",
        FeatureName: "flywheelWeight6",
        GroupIndex: 2,
        Product1Value: "16 LBS",
        Product2Value: "10 LBS",
        SortOrder: 65
      }, {
        DisplayName: "Drive 6",
        FeatureName: "drive6",
        GroupIndex: 2,
        Product1Value: "Rear",
        Product2Value: "Front",
        SortOrder: 75
      }, {
        DisplayName: "Product Style 7",
        FeatureName: "productStyle7",
        GroupIndex: 2,
        Product1Value: "Elliptical",
        Product2Value: "Elliptical",
        SortOrder: 16
      }, {
        DisplayName: "Flywheel Weight 7",
        FeatureName: "flywheelWeight7",
        GroupIndex: 2,
        Product1Value: "16 LBS",
        Product2Value: "10 LBS",
        SortOrder: 66
      }, {
        DisplayName: "Drive 7",
        FeatureName: "drive7",
        GroupIndex: 2,
        Product1Value: "Rear",
        Product2Value: "Front",
        SortOrder: 76
      }]
    }, {
      title: 'Tab 2',
      active: false,
      contentTemplate: '/app/modules/builder/templates/featureCompare.html',
      specComparison: [{
        DisplayName: "Hulahoop 1",
        FeatureName: "hulahoop1",
        GroupIndex: 0,
        Product1Value: "Elliptical",
        Product2Value: "Elliptical",
        SortOrder: 10
      }, {
        DisplayName: "Bobble Head 1",
        FeatureName: "bobblehead1",
        GroupIndex: 0,
        Product1Value: "16 LBS",
        Product2Value: "10 LBS",
        SortOrder: 60
      }, {
        DisplayName: "Balloon 1",
        FeatureName: "balloon1",
        GroupIndex: 0,
        Product1Value: "Rear",
        Product2Value: "Front",
        SortOrder: 70
      }, {
        DisplayName: "Hulahoop 2",
        FeatureName: "hulahoop2",
        GroupIndex: 0,
        Product1Value: "Elliptical",
        Product2Value: "Elliptical",
        SortOrder: 11
      }, {
        DisplayName: "Bobble Head 2",
        FeatureName: "bobblehead2",
        GroupIndex: 0,
        Product1Value: "16 LBS",
        Product2Value: "10 LBS",
        SortOrder: 61
      }, {
        DisplayName: "Balloon 2",
        FeatureName: "balloon2",
        GroupIndex: 0,
        Product1Value: "Rear",
        Product2Value: "Front",
        SortOrder: 71
      }, {
        DisplayName: "Hulahoop 3",
        FeatureName: "hulahoop3",
        GroupIndex: 0,
        Product1Value: "Elliptical",
        Product2Value: "Elliptical",
        SortOrder: 12
      }, {
        DisplayName: "Bobble Head 3",
        FeatureName: "bobblehead3",
        GroupIndex: 0,
        Product1Value: "16 LBS",
        Product2Value: "10 LBS",
        SortOrder: 62
      }, {
        DisplayName: "Balloon 3",
        FeatureName: "balloon3",
        GroupIndex: 0,
        Product1Value: "Rear",
        Product2Value: "Front",
        SortOrder: 72
      }, {
        DisplayName: "Hulahoop 4",
        FeatureName: "hulahoop4",
        GroupIndex: 0,
        Product1Value: "Elliptical",
        Product2Value: "Elliptical",
        SortOrder: 13
      }, {
        DisplayName: "Bobble Head 4",
        FeatureName: "bobblehead4",
        GroupIndex: 0,
        Product1Value: "16 LBS",
        Product2Value: "10 LBS",
        SortOrder: 63
      }, {
        DisplayName: "Balloon 4",
        FeatureName: "balloon4",
        GroupIndex: 0,
        Product1Value: "Rear",
        Product2Value: "Front",
        SortOrder: 73
      }, {
        DisplayName: "Hulahoop 5",
        FeatureName: "hulahoop5",
        GroupIndex: 1,
        Product1Value: "Elliptical",
        Product2Value: "Elliptical",
        SortOrder: 14
      }, {
        DisplayName: "Bobble Head 5",
        FeatureName: "bobblehead5",
        GroupIndex: 1,
        Product1Value: "16 LBS",
        Product2Value: "10 LBS",
        SortOrder: 64
      }, {
        DisplayName: "Balloon 5",
        FeatureName: "balloon5",
        GroupIndex: 1,
        Product1Value: "Rear",
        Product2Value: "Front",
        SortOrder: 74
      }, {
        DisplayName: "Hulahoop 6",
        FeatureName: "hulahoop6",
        GroupIndex: 1,
        Product1Value: "Elliptical",
        Product2Value: "Elliptical",
        SortOrder: 15
      }, {
        DisplayName: "Bobble Head 6",
        FeatureName: "bobblehead6",
        GroupIndex: 2,
        Product1Value: "16 LBS",
        Product2Value: "10 LBS",
        SortOrder: 65
      }, {
        DisplayName: "Balloon 6",
        FeatureName: "balloon6",
        GroupIndex: 2,
        Product1Value: "Rear",
        Product2Value: "Front",
        SortOrder: 75
      }, {
        DisplayName: "Hulahoop 7",
        FeatureName: "hulahoop7",
        GroupIndex: 2,
        Product1Value: "Elliptical",
        Product2Value: "Elliptical",
        SortOrder: 16
      }, {
        DisplayName: "Bobble Head 7",
        FeatureName: "bobblehead7",
        GroupIndex: 2,
        Product1Value: "16 LBS",
        Product2Value: "10 LBS",
        SortOrder: 66
      }, {
        DisplayName: "Balloon 7",
        FeatureName: "balloon7",
        GroupIndex: 2,
        Product1Value: "Rear",
        Product2Value: "Front",
        SortOrder: 76
      }]
    }, {
      title: 'Tab 3',
      active: false,
      contentTemplate: 'myTemplate.htm' //uses $templateCache above
    }];

    /*--------Panel Buttons------------*/
    $scope.panelssm = [{
      target: 'builder.BuildHub',
      text: 'Learn',
      icon: 'fa fa-graduation-cap'
    }, {
      target: 'builder.PhoneCountries',
      text: 'Build',
      icon: 'fa fa-th-large'
    }];

    $scope.panelsmd = [{
      target: 'builder.BuildHub',
      text: 'TV',
      image: 'images/common/images/smart-tv.png'
    }, {
      target: 'builder.PhoneCountries',
      text: 'Phone',
      image: 'images/common/images/smart-phone.png'
    }];

    $scope.panelslg = [{
      target: 'builder.BuildHub',
      text: 'U.S. and Canada',
      image: '/images/builder/images/phone-international-thumbnail.jpg'
    }, {
      target: 'builder.PhoneCountries',
      text: 'International',
      image: '/images/builder/images/phone-national-thumbnail.jpg'
    }];





    $scope.radio1 = '2';

    //small button set
    $scope.check1 = [{
      id: 'biking',
      text: 'Biking',
      selected: false
    }, {
      id: 'cardio',
      text: 'Cardio Training',
      selected: false
    }, {
      id: 'classes',
      text: 'Exercise Classes',
      selected: false
    }, {
      id: 'hiking',
      text: 'Hiking',
      selected: false
    }, {
      id: 'pt',
      text: 'Physical Therapy',
      selected: false
    }, {
      id: 'running',
      text: 'Running',
      selected: false
    }, {
      id: 'team',
      text: 'Team Sports',
      selected: false
    }, {
      id: 'walking',
      text: 'Walking',
      selected: false
    }, {
      id: 'other',
      text: 'Other',
      selected: false
    }];

    //large button set 2x2
    $scope.check2 = [{
      id: 'ceilings',
      text: 'CEILINGS UNDER 8 FEET',
      selected: false
    }, {
      id: 'neighbors',
      text: 'DOWNSTAIRS NEIGHBORS',
      selected: false
    }, {
      id: 'move',
      text: 'NEED TO MOVE EQUIPMENT FREQUENTLY',
      selected: false
    }, {
      id: 'space',
      text: 'SMALL SPACE AVAILABLE',
      selected: false
    }];

    //large button radio set 3x1

    $scope.radio2 = {
      options: [{
        id: 'little',
        text: 'LESS THAN 1 HOUR PER DAY',
        selected: false
      }, {
        id: 'medium',
        text: '1 TO 2 HOURS PER DAY',
        selected: false
      }, {
        id: 'lots',
        text: 'OVER 2 HOURS PER DAY',
        selected: false
      }],
      selected: 'little'
    };



    $scope.product1 = {
      name: 'FREEMOTION 861',
      saPrice: 1499.99,
      retailPrice: 2999.99,
      bestFit: true,
      availableToTry: true
    };

  }


})(angular);
