var gulp = require('gulp'),
  sass = require('gulp-sass'),
  concat = require('gulp-concat'),
  defaultPlumber = require('../util/default.plumber.js'),
  browserSync = require('browser-sync'),
  files = require('../config/files.config.js'),
  autoprefixer = require('gulp-autoprefixer'),
  gulpConfig = require('../config/gulp.config');


var reload = browserSync.reload,
    isDebug = gulpConfig.debug || gulpConfig.debugStyles;

if(isDebug){
  gulpConfig.sass = gulpConfig.sass || {};

  gulpConfig.sass.outputStyle = 'expanded';
  gulpConfig.sass.sourceComments = true;
}

gulpConfig.autoprefixer = gulpConfig.autoprefixer || {};

gulp.task('styles', function () {

    return  gulp.src(files.styles.client)
        .pipe(defaultPlumber())
        .pipe(sass(gulpConfig.sass))
        .pipe(autoprefixer(gulpConfig.autoprefixer))
        .pipe(concat(gulpConfig.angularApp + '.min.css'))
        .pipe(gulp.dest(gulpConfig.buildDir + '/css'))
        .pipe(reload({ stream: true }));
    });
