(function(ng) {
  'use strict';

  ng.module('client.home').config(config);

  /* @ngInject */
  function config($stateProvider) {

    $stateProvider.state('home', {
      url: '/',
      config:{
      },
      templateUrl: 'app/home/home.html',
      controller: 'HomeCtrl',
      controllerAs: 'homeVM'
    });
  }

})(angular);
