var optional = require("optional"),
  _ = require("lodash"),
  args = require('yargs').argv;

var config = optional("./gulp.config") || {};
var dest =  config.buildDir || 'build';

var src = './src';

var defaults  = {
  buildDir: dest ,
  angularApp: 'client',
  angularAppTemplates: 'client.templates',
  debug: args.debug,
  debugStyles: args.debugStyles,
  debugDist: args.debugDist,
  default: ['build', 'watch','tdd'], /* default build command, default is the one used to package core*/
  build: ['appsettings', 'version','styles', 'generate-templates', 'copy', 'images'], /* default build command, default is the one used to package core*/
  browserSync: {
		server: {
			baseDir: dest,
		}
	},
  sass: {
      errLogToConsole: true,
      outputStyle:'compressed',
      includePaths: ['js', 'bower_components']
  },
  version:{
    variable: 'ClientVersion',
    constant: 'ClientVersion',
    module: 'client.version',
  },
  autoprefixer:{
    browsers: ['last 2 versions']
  }
};

module.exports = _.merge(defaults, config || {}, function(a, b) {
  if (_.isArray(b)) {
    return b;
  }
});

if(defaults.debug === false && args.debug ){
  //override "false", if --debug is present
  module.exports.debug = true;
}
if(defaults.debugStyles === false && args.debugStyles ){
  //override "false", if --debug is present
  module.exports.debugStyles = true;
}

if(defaults.debugDist === false && args.debugDist ){
  //override "false", if --debug is present
  module.exports.debugDist = true;
}


if(module.exports.debug){
  console.log('##########################');
  console.log("### gulp in debug mode ###");
  console.log('##########################');
}

if(module.exports.debugStyles){
  console.log('##########################');
  console.log("### gulp in debugStyles mode ###");
  console.log('##########################');
}

if(module.exports.debugDist){
  console.log('##########################');
  console.log("### gulp in debugDist mode ###");
  console.log('##########################');
}
