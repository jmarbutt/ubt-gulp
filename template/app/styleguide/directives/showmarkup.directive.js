(function(angular) {
  'use strict';
  //TODO move this to its own module.
  angular.module('client.styleguide')
    .directive('showMarkupTarget', ShowMarkupTargetDirective)
    .directive('showMarkup', ShowMarkupDirective);


    /* @ngInject */
    function ShowMarkupTargetDirective() {

      return {
        compile: compileFunc,
        priority:100000.1,
        require:'^showMarkup'
      };

      function compileFunc(iElem, iAttr) {
        var findMe=iElem[0].outerHTML;
        return function(scope, element, attrs, ctrl, transclude)  {
              scope.$parent.originalHTML = findMe;
        };
      }

    }

  /* @ngInject */
  function ShowMarkupDirective() {

    return {
      templateUrl: 'app/styleguide/showmarkup.directive.html',
      replace: true,
      priority:100000,
      transclude: true,
      controller: MyController,
      scope:{

      }
    };


  }
  /* @ngInject */
  function MyController($scope){
    var  _showMarkup = true;
    $scope.isVisible = isVisible;
    $scope.toggleVisible = toggleVisible;

    function toggleVisible(){
      _showMarkup = !_showMarkup;
    }

    function isVisible(){
      return _showMarkup === true;
    }

  }


})(angular);
