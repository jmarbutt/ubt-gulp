var defaults = {
    baseGulpPath: __dirname,
    styles: {
      client: [ 'app/client.scss'],
      watch: ['app/**/*.scss']
    },
    index:['index.html'],
    scripts: {
      client: ['app/**/_*.js',
               'app/**/*.js'
      ],
      test: ['app/**/*.spec.js', 'app/**/*.*.spec.js'],
      dist: [
        'bower_components/ionic/release/js/ionic.js',
        'bower_components/angular/angular.js',
        'bower_components/angular-animate/angular-animate.js',
        'bower_components/angular-sanitize/angular-sanitize.js',
        'bower_components/angular-touch/angular-touch.js',
        'bower_components/angular-messages/angular-messages.js',
        'bower_components/angular-resource/angular-resource.js',
        'bower_components/angular-ui-router/release/angular-ui-router.js',
        'bower_components/ionic/release/js/ionic-angular.js',
        'bower_components/momentjs/min/moment.min.js',
        'bower_components/underscore/underscore.js',
        'bower_components/ubt-angular-core/dist/ubt-angular-core.min.js',
        'bower_components/angular-bootstrap/ui-bootstrap-tpls.js'
      ]
    },
    fonts: [
      'bower_components/bootstrap-sass/assets/fonts/**',
      'bower_components/font-awesome/fonts/**',
      'fonts/**'
    ],
    assets:['cordova.js',
	           'web.config',
            'favicon.ico',
            'bower_components/open-sans-fontface/**/*.{ttf,svg,woff,woff2,,eot}',
            'app/**/*.{svg,png,jpg,jpeg,eot,ttf,svg,woff,svg}'],
    templates:{
      client: ['app/**/*.htm','app/**/*.html'],
    },
};


module.exports = defaults;
