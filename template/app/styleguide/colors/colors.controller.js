(function(angular) {
  'use strict';
  angular.module('client.styleguide')
    .controller('ColorsCtrl', ColorsCtrl);
  /* @ngInject */
  function ColorsCtrl($scope) {
    $scope.isRgb = isRgb;
    $scope.rgbToHex = rgbToHex;
    $scope.isHex = isHex;

    $scope.standardize = standardize;

     function standardize(color) {
       if (isRgb(color)) {
         return rgbToHex(color).toUpperCase();
       }
       return color;
     }

    function isRgb(color) {
      if(!color){return;}
      return /^(rgb[(])?([0-2][0-5][0-5])|(\d?\d), ?([0-2][0-5][0-5])|(\d?\d), ?([0-2][0-5][0-5])|(\d?\d)[)]?$/g.test(color.toLowerCase());
    }

    function isHex(color) {
      if(!color){return;}
      return /^#?[\da-fA-F]{3}([\da-fA-F]{3})?$/g.test(color.toLowerCase());
    }

    function rgbToHex(rgb) {
      var _rgb = rgb.replace(/[rgb() ]+/g, "").split(",");
        /* /([0-2]?[0-5]?\d)/.exec(rgb), */
      var  r = parseInt(_rgb[0]),
        g = parseInt(_rgb[1]),
        b = parseInt(_rgb[2]);

      return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
    }
  }

})(angular);
