var jshint = require('gulp-jshint'),
  gulp = require('gulp'),
  stylish = require('jshint-stylish'),
  files = require('../config/files.config.js'),
  fs = require('fs');


function getJSHint(single){
  //start with client.  if it dne fallback to core
  var jshintfile = files.baseGulpPath + '.jshintrc';
  console.log("Searching for jshintfile ", jshintfile);
  if (!fs.existsSync(jshintfile)) {
      // Do something
      jshintfile = files.corePath + 'gulp/.jshintrc';
      console.warn('Failed to find client jshintfile, falling back to core:',jshintfile);
      if(!fs.existsSync(jshintfile)){
        console.error('failed to jshintfile file', jshintfile);
        throw 'jshint.js: failed to find jshintfile file';

      }
  }
  return jshintfile;
}

gulp.task('watch-jshint', ['jshint'], function () {
    gulp.watch(_getFiles(), ['jshint']);
});

function _getFiles(){
  return files.scripts.jshint || files.scripts.client;
}

var theFile;
gulp.task('jshint', function () {
    console.log("Checking jshint");
    theFile = theFile || getJSHint();
    return gulp.src(_getFiles())
      .pipe(jshint(theFile))
      .pipe(jshint.reporter(stylish));
});
