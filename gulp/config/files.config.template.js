var corePath =  __dirname + '/bower_components/assistantcore/';
var files = require(corePath + 'gulp/config/files.config'),
    _ = require('lodash');

function _wrap(prefix, files){
    return _.map(files, function(item){
      var negate = '';
      if(item.indexOf('!') === 0){
        item = item.substring(1,item.length);
        negate = '!';
      }
      //this is a hack right now bc gulp config doesn't read config currectly so path is not prepended to it.
      return negate + prefix + item;
    });
}

var defaults = {
    baseGulpPath: __dirname,
    styles: {
      client: [ 'app/app.scss'],
      watch: ['app/**/*.scss'].concat(_wrap(corePath, ['app/**/*.scss']))
    },
    index:['index.html'],
    scripts: {
      core: _wrap(corePath, files.scripts.core),
      client: ['app/**/_*.js',
        'app/**/*.js'
      ],
      test: ['app/**/*.spec.js', 'app/**/*.*.spec.js'],
      dist: [
        'bower_components/ionic/release/js/ionic.js',
        'bower_components/angular/angular.js',
        'bower_components/angular-animate/angular-animate.js',
        'bower_components/angular-sanitize/angular-sanitize.js',
        'bower_components/angular-touch/angular-touch.js',
        'bower_components/angular-messages/angular-messages.js',
        'bower_components/angular-resource/angular-resource.js',
        'bower_components/angular-ui-router/release/angular-ui-router.js',
        'bower_components/ionic/release/js/ionic-angular.js',
        'bower_components/fastclick/lib/fastclick.js',
        'bower_components/CryptoJS/build/rollups/pbkdf2.js',
        'bower_components/angular-ui-utils/ui-utils.js',
        'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
        'bower_components/momentjs/min/moment.min.js',
        'bower_components/angular-ui-grid/ui-grid.js',
        'bower_components/underscore/underscore.js',
        'bower_components/ubt-angular-core/dist/ubt-angular-core.min.js',
      ]
    },
    fonts: [
      'bower_components/assistantcore/app/fonts/*.{ttf,svg,woff}',
      'bower_components/bootstrap-sass/assets/fonts/**', //i have to do this bc they dont have a standard format one is font/bootstrap and the other is font-awsome/fonts
      'bower_components/font-awesome/fonts/**',
      'fonts/**'
    ],
    assets:[
      'cordova.js',
	     'web.config',
      'favicon.ico',
      'bower_components/open-sans-fontface/**/*.{ttf,svg,woff,woff2,,eot}',
      'app/**/*.{svg,png,jpg,jpeg,eot,ttf,svg,woff,svg}'],
    templates:{
      client: ['app/**/*.htm','app/**/*.html'],  /* dont like this wnated to use {x,y} but karma is barfing with mtime error */
      core: _wrap(corePath, files.templates.core)
    },
    corePath: corePath
};


module.exports = defaults;
