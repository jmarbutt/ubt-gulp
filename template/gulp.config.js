var defaults  = {
  default: ['browserSync', 'tdd', 'watch'],
  build: [
    'version',
    'appsettings',
    'generate-templates',
    'styles',
    'scripts',
    'copy',
    'images',
    'inject'
  ],
  debug: false ,
  debugDist:  false,
  debugStyles:  false

};

module.exports = defaults;
