(function(ng) {
  'use strict';

  ng.module('client.styleguide').config(config);

  /* @ngInject */
  function config($stateProvider) {

    var states = [
      'buttons',
      'colors',
      'typography',
      'validation'
    ];


    $stateProvider.state('styleguide', {
      url: '/styleguide/',
      config:{
        noLoginRequired: true,
      },
      redirectTo: 'styleguide.colors',
      templateUrl: 'app/styleguide/styleguide.html',
      controller: 'StyleGuideCtrl',
    });

    _.each(states, function(name){
      $stateProvider.state('styleguide.' + name , {
        url: name + '/',
        config:{
          noLoginRequired: true,
        },
        controller:  name[0].toUpperCase() + name.slice(1) + 'Ctrl',
        templateUrl: 'app/styleguide/' +   name + '/' + name+ '.html',
      });

    });

  }

})(angular);
