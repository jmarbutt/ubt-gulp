(function(ng, AppSettings) {
  'use strict';

  ng.module('client').config(config);

  /* @ngInject */
  function config($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise(function(injector, hash){
        return '/';
    });

    $urlRouterProvider.rule(function ($injector, $location) {
        var path = $location.url();

        // check to see if the path already has a slash where it should be
        if (path[path.length - 1] === '/' || path.indexOf('/?') > -1) {
            return;
        }

        if (path.indexOf('?') > -1) {
            return path.replace('?', '/?');
        }

        return path + '/';
    });

  }

})(angular, window.AppSettings);
