(function(angular) {
  'use strict';
  //TODO move this to its own module.
  angular.module('client.styleguide')
    .directive('matchWidth', MatchWidthDirective);


    /* @ngInject */
    function MatchWidthDirective() {

      return compileFunc;

      function compileFunc(scope, element, iAttr) {

        scope.$watch(function(){
          element.css('height',element[0].offsetWidth + 'px');
        });

      }

    }


})(angular);
