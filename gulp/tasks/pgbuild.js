// This script does all the steps to get a build through PhoneGap Build.
// It determines what to do based on the settings in app_settings.js and
// the PhongGap/pgbuild-config-{PROFILE}.js file for the applicable profile.

var gulp = require('gulp'),
  gulpConfig = require('../config/gulp.config'),
  file = require('gulp-file'),
  versionTask = require('./version'),
  optional = require("optional"),
  runSequence = require('run-sequence'),
  request = require('request'); // See https://github.com/mikeal/request for help on using the request module


gulp.task('pgbuild', function(callback){
  runSequence('build',  'copy-phonegap', 'pgbuild-package', callback);
});

gulp.task('pgbuild-package',  function (done) {

  //Use this to turn on request debugging when needed
  //require('request-debug')(request);

  var zip = require('gulp-zip'),
      del = require('del'),
      fs = require('fs'),
      Q = require('q'),
      pgClient = require('phonegap-build-api'),
      gulpAwsS3 = require('gulp-aws-s3'),
      runSequence = require('run-sequence'),
      unzip = require('gulp-unzip'),
      minimatch = require('minimatch');

  //We get profile, version number and other information from the app's settings
  var appSettings = require('../config/app.config'),
      files = require('../config/files.config');

  var version = require(files.baseGulpPath + 'bower.json');


  //We use the config file that's associated with current profile in AppSettings

  if (appSettings.ENABLE_PGBUILD) {
      config = optional(files.baseGulpPath + 'PhoneGap/pgbuild-config-' + appSettings.PROFILE + '.js');

      if(!config){
        console.log("  ");
        console.log("  ");
        console.log("  ");
        console.error("Warning.. could not find: " , files.baseGulpPath + 'PhoneGap/pgbuild-config-' + appSettings.PROFILE + '.js' );
        console.error('PhoneGap will be disabled');
        console.log("  ");
        console.log("  ");
        console.log("  ");
      return;
    }
  }

  //Figure out some globals from the settings and config files
  //var buildPadded = ('0000' + version.version).slice(-4);
  var pgbuildOutDir = 'pgbuild';
  var pgbuildZipFile = 'pgbuild.zip';
  var filePrefix = appSettings.APP_PREFIX + version.version;
  var ipaFile = filePrefix + '-' + appSettings.PROFILE + '.ipa';
  var pListFile = filePrefix + '-' + appSettings.PROFILE +  '-Info.plist';
  var apkFile = filePrefix + '-' + appSettings.PROFILE + '.apk';


    if (!appSettings.ENABLE_PGBUILD) {
        console.log('PGBuild is not enabled for the current profile in app-settings.js');
        return;
    }
    var deferred = Q.defer();

    //Create the list of tasks based on the config file
    // (every task should return a promise)
    var funcs = [];

    setupZipStep(funcs);

    setupBuildStep(funcs);

    setupDownloadStep(funcs);

    setupUploadStep(funcs);

    setupRegisterStep(funcs);

    setupGenerateIPAStep(funcs);

    funcs.push(done);

    //Run the queue
    var queue = Q();
    funcs.forEach(function (f) {
      queue = queue.then(f).fail(_failed);
    });
    queue.then(function () {
        deferred.resolve();
    }).fail(_failed);
    function _failed(){
      process.stdout.write('Failed... ' + arguments);
      process.exit(1);
    }

    return deferred.promise;




function setupGenerateIPAStep(funcs){
  if (!config.steps.generatePlist) {  return;}

  funcs.push(uploadPlistToS3);
}

function setupZipStep(funcs){
  if (!config.steps.zip) {  return;}

  funcs.push(doZip);
}

function setupUploadStep(funcs){
  if(!config.steps.uploadS3){ return ;}

  if (config.platforms.ios) {
      funcs.push(uploadToS3iOS);
  }
  if (config.platforms.android) {
      funcs.push(uploadToS3Android);
  }
}

function setupRegisterStep(funcs){

  if(!config.steps.register){ return ;}


  if (config.platforms.ios) {
      funcs.push(registerVersioniOS);
  }
  if (config.platforms.android) {
      funcs.push(registerVersionAndroid);
  }

}

function setupDownloadStep(funcs){
  if(!config.steps.download){ return ;}

  if (config.platforms.ios) {
      funcs.push(downloadFromPGBuildiOS);
      funcs.push(validateDownloadedVersioniOS);
  }
  if (config.platforms.android) {
      funcs.push(downloadFromPGBuildAndroid);
      funcs.push(validateDownloadedVersionAndroid);
  }
}

function setupBuildStep(funcs){
  if(!config.steps.build){ return ;}

  if (config.platforms.ios) {
      funcs.push(unlockiOS);
  }
  funcs.push(uploadToPGBuild);
  if (config.platforms.ios) {
      funcs.push(waitForBuildiOS);
  }
  if (config.platforms.android) {
      funcs.push(waitForBuildAndroid);
  }

}
function doZip() {

    var deferred = Q.defer();
    //Zips up all the files we want and puts the zip file in the pgbuild folder
    process.stdout.write('Zipping contents to ' + pgbuildOutDir + '/' + pgbuildZipFile + '...');

    gulp.src([
        gulpConfig.buildDir + '/**/*',
        '!**/*.orig',
        '!**/spec.js', '!**/spec.min.js'
    ], { base: gulpConfig.buildDir })
    .pipe(zip(pgbuildZipFile))
    .pipe(gulp.dest(pgbuildOutDir))
    .on('end', function() {
      console.log('Complete');
      deferred.resolve();
    });
    return deferred.promise;
}

function unlockiOS() {
    var deferred = Q.defer();
    process.stdout.write('Unlocking iOS key...');
    var requestOptions = {
        url: 'https://build.phonegap.com/api/v1/keys/ios/' + config.pgbuild.iOSsigningKeyID + '?auth_token=' + config.pgbuild.token,
        json: { data : { 'password': config.pgbuild.iOSsigningKeyPassword}}
    };
    request.put(requestOptions, function (error, response, body) {
        if (!error && response.statusCode == 202) {
            if (body.locked === false) {
                console.log('Complete');
                deferred.resolve();
            } else {
                console.log('Error: The API to unlock the signing key reported success but the key is still locked');
                deferred.reject();
            }
        } else {
            console.log('Error unlocking key');
            console.log(error);
            deferred.reject();
        }
    });

    return deferred.promise;

}

function uploadToPGBuild() {
    var deferred = Q.defer();
    //Zips up all the files we want and puts the zip file in the pgbuild folder
    process.stdout.write('Uploading zip file to PhoneGap Build(' +
            config.pgbuild.appID + ') ...' +
            files.baseGulpPath  +  pgbuildOutDir + '/' + pgbuildZipFile);

    pgClient.auth({ token: config.pgbuild.token }, function(e, api) {
      // use `api` to make requests
      if(e){
        console.error("failed to connect");
        return  q.reject('failed to connect');
      }
      var options = {
          form: {
            data : {
                'debug': config.pgbuild.debug,
                'version': version.version,
                'description': config.pgbuild.description || ('Profile: ' +  appSettings.PROFILE) //this will be overrriden if specificed in config.xml
            },
            file: files.baseGulpPath  +  pgbuildOutDir + '/' + pgbuildZipFile
          }
      };
      console.log("Settings:", options);

      api.put('/apps/' + config.pgbuild.appID, options, function(e, data) {
          console.log('error:', e);
          console.log('data:', data);

          if (!e) {
              console.log('Build started');
              deferred.resolve(data);
          } else {
              console.log('Error uploading the file', e);
              console.log(data, e);
              deferred.reject();
          }

      });
    });

    return deferred.promise;
}

function waitForBuildiOS() {
    return waitForBuild('ios');
}
function waitForBuildAndroid() {
    return waitForBuild('android');
}

function waitForBuild(platform){
    var deferred = Q.defer();
    process.stdout.write('Waiting for ' + platform + ' build to complete...');
    var requestOptions = {
        url: 'https://build.phonegap.com/api/v1/apps/' + config.pgbuild.appID + '?auth_token=' + config.pgbuild.token,
        json: true
    };

    function setupTimeout() {
         setTimeout(function () {
            process.stdout.write('.');
            console.log(requestOptions);
            request(requestOptions, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                  if (body.status[platform] == 'skip') {
                      console.log('skip');
                      deferred.resolve();
                  }else  if (body.status[platform] == 'complete') {
                        console.log('Complete');
                        deferred.resolve();
                  }else if (body.status[platform] == 'error') {
                      console.log('Error during build: ' + platform);
                      console.log(error);
                      console.log(body);
                      deferred.reject();
                  } else {
                        console.log(body.status);
                        setupTimeout();
                  }
                }else{
                    console.log('Error checking for build status');
                    console.log(error);
                    console.log(body);
                    deferred.reject();
                }
            });
        }, 2000);
    }

    setupTimeout();
    return deferred.promise;

}

function downloadFromPGBuildiOS() {
    return downloadFromPGBuild('ios', pgbuildOutDir + '/' + ipaFile);
}

function downloadFromPGBuildAndroid() {
    return downloadFromPGBuild('android', pgbuildOutDir + '/' + apkFile);
}

function downloadFromPGBuild(platform, filePath) {
    var deferred = Q.defer();
    process.stdout.write('Downloading build to local computer...');

    var requestOptions = {
        url: 'https://build.phonegap.com/api/v1/apps/' + config.pgbuild.appID + '/' + platform + '?auth_token=' + config.pgbuild.token
    };

    request.get(requestOptions, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log('Complete');
            deferred.resolve();
        } else {
            console.log('Error downloading file');
            console.log(error);
            console.log(response.status);
            console.log(response);
            deferred.reject();
        }
    })
    .pipe(fs.createWriteStream(filePath));

    return deferred.promise;

}

function validateDownloadedVersioniOS() {
    return validateDownloadedVersion('ios',pgbuildOutDir + '/' + ipaFile);
}
function validateDownloadedVersionAndroid() {
    return validateDownloadedVersion('android', pgbuildOutDir + '/' + apkFile);
}
function validateDownloadedVersion(platform, filePath){
    //Looks in the zip file for the app_settings and makes sure the version match
    var deferred = Q.defer();
    process.stdout.write('Validating downloaded file...');

    var outDir = pgbuildOutDir + '/validate';
    var entryPath, versionFile;

    //Delete the old out dir
    del(outDir, function () {
        //Pull the app-settings out of the zip file and put in the out dir
        gulp.src(filePath)
         .pipe(unzip({
             filter: function (entry) {
                 if (minimatch(entry.path, '**/app.settings.js')) {
                     entryPath = entry.path;
                     return true;
                 } else if (minimatch(entry.path, '**/bower.json')) {
                      versionFile = entry.path;
                      return true;
                  } else {
                     return false;
                 }
             }
         }))
        .pipe(gulp.dest(outDir))
        .on('end', function () {

            if (!entryPath) {
                console.log('Error -- app-settings.js not found in downloaded file');
                deferred.reject();
                return;
            }
            //Now get the app-settings that was found
            var downloadedSettings = require(files.baseGulpPath +  outDir + '/' + entryPath);
            if (downloadedSettings.PROFILE != appSettings.PROFILE ||
                (!versionFile || (versionFile.version != downloadedSettings.version))
              ) {
                console.log('ERROR!');
                console.log('The downloaded file does not have the same app-settings.js file as the source');
                deferred.reject();
            }

            console.log('Complete');
            deferred.resolve();
        });
    });
    return deferred.promise;

}

function uploadToS3iOS() {
    console.log("upload to sw"  +  pgbuildOutDir);
    return uploadToS3(pgbuildOutDir + '/' + ipaFile);
}

function uploadToS3Android() {
    return uploadToS3(pgbuildOutDir + '/' + apkFile);
}

function uploadPlistToS3(fileName){
  var deferred = Q.defer();
  console.log('Uploading to S3 (this will be asynchronous)');
  console.log('Watch for the beep and S3::putObject send message to know the transfer is complete');


  console.log("creating plist file", pListFile);
  var strFile  = createPlistFile(
      _getS3FileName(ipaFile),
      config.pgbuild.bundleIdentifier,
      config.pgbuild.appName);


      file(pListFile, strFile, { src: true })
        .pipe(gulpAwsS3.upload(
          {
              acl: (config.s3.secureFile ? 'private' : 'public-read')
          }, {
              key: config.s3.accessKey,
              secret: config.s3.secretAccessKey,
              bucket: config.s3.bucket,
              region: config.s3.region
          }));

          console.log(strFile);

      //Couldn't find a way to make this plugin or gulp-s3 to callback when
      //done so we just have to say it's good for now
      //(Correct solution may be to make custom branch off gulp-aws-s3 that does it synchronously like we want)
      deferred.resolve();
      return deferred.promise;


}
function uploadToS3(filePath) {
    var deferred = Q.defer();
    console.log('Uploading to S3 (this will be asynchronous)');
    console.log('Watch for the beep and S3::putObject send message to know the transfer is complete');

    gulp.src(filePath)
    .pipe(gulpAwsS3.upload(
        {
            acl: (config.s3.secureFile ? 'private' : 'public-read')
        }, {
            key: config.s3.accessKey,
            secret: config.s3.secretAccessKey,
            bucket: config.s3.bucket,
            region: config.s3.region
        }));

    //Couldn't find a way to make this plugin or gulp-s3 to callback when
    //done so we just have to say it's good for now
    //(Correct solution may be to make custom branch off gulp-aws-s3 that does it synchronously like we want)
    deferred.resolve();
    return deferred.promise;

}

function registerVersioniOS() {
    return registerVersion('ios', ipaFile);
}

function registerVersionAndroid() {
    return registerVersion('android', apkFile);
}

/* if there is a value add the trailing slash .  Falsy will not do anything */
function _addTrailing(strVal){
  if(!strVal){
    return '';
  }
  return strVal.replace(/\/?$/, '/');
}


function createPlistFile(fileUrl, bundleIdentifier, appName){
    return  ('<?xml version="1.0" encoding="UTF-8"?> ' +
    '<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">' +
    '<plist version="1.0">' +
    '  <dict>' +
    '    <key>items</key>' +
    '    <array>' +
    '      <dict>' +
    '        <key>assets</key>' +
    '        <array>' +
    '          <dict>' +
    '            <key>kind</key>' +
    '            <string>software-package</string>' +
    '            <key>url</key>' +
    '            <string>{fileUrl}</string> '+
    '          </dict>' +
    '        </array>' +
    '        <key>metadata</key>' +
    '        <dict>' +
    '          <key>bundle-identifier</key>' +
    '          <string>{bundleIdentifier}</string>' +
    '          <key>kind</key>' +
    '          <string>software</string>' +
    '          <key>subtitle</key>' +
    '          <string>Subtitle</string>' +
    '          <key>title</key>' +
    '          <string>{appName}</string>' +
    '        </dict>' +
    '      </dict>' +
    '    </array>' +
    '  </dict>' +
    '</plist>').replace('{fileUrl}', fileUrl).replace('{bundleIdentifier}', bundleIdentifier).replace('{appName}', appName);
    }


function _getS3FileName(fileName){
    return 'https://s3.amazonaws.com/' + config.s3.bucket  + '/'+  _addTrailing(config.s3.bucketDir) + fileName;
}
/*
*  This will hit an api to register a new version.  This uses a "basic" authentiation
*  token (apiKey) that we expect the api to validate to make sure this client is authorized
*  it poststhe platform, status, version (object major, minor, build, and version string), and url to download
*
*/
function registerVersion(platform, fileName) {
    var deferred = Q.defer();
    var status = config.register.status;
    process.stdout.write('Registering version with status "' + status + '"...' );

    var url = 'https://s3.amazonaws.com/' + config.s3.bucket  + '/'+  _addTrailing(config.s3.bucketDir) + fileName;
    var requestOptions = {
        url: config.register.url,
        form: {
            apikey: config.register.apiKey,
            platform: platform,
            status: status,
            version: versionTask.getVersionObject(version.version),
            url: url
        }
    };
    request.post(requestOptions, function (error, response, body) {
        if (!error && response.statusCode == 200) {

            var result = JSON.parse(body);
            if (result.success === false) {
                console.log('Error:');
                console.log(result);
            } else {
                console.log('Complete');

                deferred.resolve();
            }
        } else {
            console.log('Error registering version', arguments);
            console.log(error);
            console.log(body);
            deferred.reject();
        }
    });

    return deferred.promise;
}

function done() {
    console.log('DONE');
    var deferred = Q.defer();
    deferred.resolve();
    return deferred.promise;

}
});
