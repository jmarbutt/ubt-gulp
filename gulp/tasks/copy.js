var gulp = require('gulp'),
    gulpCopy = require('gulp-copy'),
    _ = require('lodash'),
    config = require('../config/gulp.config.js'),
    files = require('../config/files.config.js'),
    replace = require('gulp-replace');

var isDebug = config.debug,
    build = config.buildDir,
    debugDist = config.debugDist;


gulp.task('copy-scripts', copyScripts);
gulp.task('copy-phonegap', ['generate-config-xml'], copyPhonegap);
gulp.task('copy-dist', ['scripts-dist'], copyDist);
gulp.task('copy-inject', ['copy-inject-files', 'copy-inject-html']);
gulp.task('copy-inject-html',  copyInjectHtml);
gulp.task('copy-inject-files', copyInjectFiles);
gulp.task('copy-templates', copyTemplates);
gulp.task('copy-fonts', copyFonts);
gulp.task('copy-assets', copyAssets);
gulp.task('copy-styles', copyStyles);

gulp.task('generate-config-xml', generateConfigXML);

gulp.task('copy',
	  [
	    'copy-assets',
	    'copy-inject',
	    'copy-fonts',
	    'copy-scripts',
	    'copy-dist',
	    'copy-templates',
	    'copy-styles',
	  ],
	  copyFiles
	 );

function copyFiles(){
  //no op
}

function generateConfigXML(){
  //take the config.xml and replace the {{bundleIdentifier}} && {{appVersion}}
  //read in the config.xml file
  var appSettings = require('../config/app.config');
  var version = require(files.baseGulpPath + 'bower.json');

  return  gulp.src(['config.xml'])
        .pipe(replace('{{bundleID}}', appSettings.BUNDLE_IDENTIFIER))
        .pipe(replace('{{appName}}', appSettings.APP_NAME))
        .pipe(replace('{{appVersion}}', version.version ))
        .pipe(gulp.dest(build));
}

function copyPhonegap(){
  return gulp.src(
    files.phoneGap)
    .pipe(gulpCopy(build));
}


function copyScripts() {
  //Only copy files if in debug (min files automatically get written to build dir)
  if(isDebug){
    return gulp.src(files.scripts.client).pipe(gulpCopy(build));
  }
  return true;
}


function copyInjectFiles() {
  var extensions = config.inject && config.inject.map;
  if(isDebug && extensions){
    var  _files = [];
    _.each(extensions, function(item){
      _files = _files.concat(item.files);
    });

    return gulp.src(_files)
      .pipe(gulpCopy(build));
  }
  return true;

}

function copyInjectHtml() {
  var extensions = config.inject && config.inject.map;
  if(isDebug && extensions){
    var _html = [];
    _.each(extensions, function(item){
      _html = _html.concat(item.html || []);
    });

    return gulp.src(_html)
      .pipe(gulp.dest(build + '/app'));

  }
  return true;

}

function copyInjectFiles() {
  var extensions = config.inject && config.inject.map;
  if(isDebug && extensions){
    var  _files = [];
    _.each(extensions, function(item){
      _files = _files.concat(item.files);
    });

    return gulp.src(_files)
      .pipe(gulpCopy(build));
  }
  return true;

}

function copyInjectHtml() {
  var extensions = config.inject && config.inject.map;
  if(isDebug && extensions){
    var _html = [];
    _.each(extensions, function(item){
      _html = _html.concat(item.html || []);
    });

    return gulp.src(_html)
      .pipe(gulp.dest(build + '/app'));

  }
  return true;

}

function copyDist() {
  if(debugDist){
    return gulp.src(files.scripts.dist)
      .pipe(gulpCopy(build));
  }
  //no op the scripts dist will do the trick
  return true;

}


function copyTemplates() {
  if(isDebug){
    return gulp.src(files.templates.client)
      .pipe(gulpCopy(build));
  }

  return true;

}

function copyFonts() {
  return gulp.src(files.fonts)
    .pipe(gulp.dest(build + '/fonts'));
}

function copyAssets() {
  return gulp.src(files.assets).pipe(gulp.dest(build));
}

function copyStyles() {
  if(isDebug) {
    return gulp.src(files.styles.copy || [])
      .pipe(gulpCopy(build));
  }
  return true;
}
