var changed    = require('gulp-changed'),
 		gulp       = require('gulp'),
    files = require('../config/files.config'),
 		gulpIf= require('gulp-if'),
 		config = require('../config/gulp.config'),
 		imagemin   = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant');

gulp.task('images', function() {
	var dest = files.baseGulpPath + config.buildDir;

  console.log('Images are being saved to', dest);
	return gulp.src(dest + '/**/*.{png,jpg,svg}')
		//.pipe(changed(dest)) // Ignore unchanged files
		.pipe(gulpIf(!config.debug, imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		}))) // Optimize
		.pipe(gulp.dest(dest));
});
