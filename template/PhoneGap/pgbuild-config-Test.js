module.exports = {
  platforms: {
    ios:      true,
	  android:  false
  },
  steps: {
  	zip:      true,
  	build:    true,
  	download: true,
  	uploadS3: true,
  	register: true,
    generatePlist: true
  },
  pgbuild: {
    token:                 "<token>",
    appID:                 "<appid>",
    iOSsigningKeyID:       "<key>",
    iOSsigningKeyPassword: "<Password>",
    bundleIdentifier:      'com.unboxedtechnology.yours',
    appName: 'UBT',
  	debug:                 false
  },
  s3: {
    accessKey:             "<key>",
  	secretAccessKey:       "<secret>",
  	bucket:                "<bucket>",
  	region:                "us-east-1"
  },
  register: {
    apiKey:   "N/A",
  	url:      "N/A",
  	status:   "O"
  }
};
